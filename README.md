Forecasting the Aare River Temperature
==============================

## What is it?

Using machine learning to forecast the temperature of the Aare river in Bern.

## Getting started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

* **Python 3.6+**

* **virtualenv**

    Needed to create Python virtual environment and install dependencies. Install with pip

    ```
    pip install virtualenv
    ```

    or see [here](https://virtualenv.pypa.io/en/latest/installation/) for additional instructions.

### Installing

Clone the repository

```
git clone git@gitlab.com:fri-ml/aareguru-forecast.git
```

and run

```bash
cd aareguru-forecast
make install
```

From here you can activate the virtual env and spin up JupyterLab as follows:

```
source ./env/bin/activate
jupyter-lab
```

**Note:** The `requirements.txt` file has been tested on macOS but probably will not work on Linux machines because the PyTorch dependencies differ. From the PyTorch [website](https://pytorch.org/get-started/locally/?source=Google&medium=PaidSearch&utm_campaign=1712418477&utm_adgroup=66821158317&utm_keyword=pytorch%20installation&utm_offering=AI&utm_Product=PYTorch&gclid=CjwKCAjwkenqBRBgEiwA-bZVtpe4xpn8EzpGXmDjQpUSrJCCgBMty__UL9JSnEwzsCEFPwuiTLkOfhoCAO0QAvD_BwE) it appears that one should be able to install by replacing the `torch` and `torchvision` lines in `requirements.txt` with:

```
torch==1.2.0+cpu
torchvision==0.4.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
```


Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is the creator's initials,
    |                         a number (for ordering), and a short "-" delimited description, e.g.
    │                         "ltu-0.1-initial-data-exploration".
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


--------
